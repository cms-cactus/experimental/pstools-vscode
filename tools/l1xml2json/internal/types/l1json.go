package types

// L1JSON represents the l1.json file
type L1JSON struct {
	Prescales L1JSONRoot `json:"prescales"`
}

// L1JSONRoot is part of L1JSON
type L1JSONRoot struct {
	Parent string                `json:"parent"`
	Algos  map[string]L1JSONAlgo `json:"algorithms"`
}

// L1JSONAlgo is part of L1JSON
type L1JSONAlgo struct {
	Comment string    `json:"//,omitempty"`
	Columns []float64 `json:"columns"`
}
