package types

import (
	"encoding/xml"
	"strconv"
	"strings"
)

// L1XML represents a prescale XML file as used in the configuration database
type L1XML struct {
	XMLName  xml.Name       `xml:"run-settings"`
	Contexts []L1XMLContext `xml:"context"`
}

// L1XMLContext is part of L1XML
type L1XMLContext struct {
	XMLName xml.Name     `xml:"context"`
	ID      string       `xml:"id,attr"`
	Params  []L1XMLParam `xml:"param"`
}

// L1XMLParam is part of L1XML
type L1XMLParam struct {
	XMLName xml.Name `xml:"param"`
	ID      string   `xml:"id,attr"`
	Type    string   `xml:"type,attr"`
	Value   string
	L1XMLPrescaleTable
}

// L1XMLPrescaleTable is part of L1XML
type L1XMLPrescaleTable struct {
	Columns L1XMLPrescaleTableColumns `xml:"columns"`
	Types   string                    `xml:"types"`
	Rows    L1XMLPrescaleTableRows    `xml:"rows"`
}

// L1XMLPrescaleTableRows is part of L1XML
type L1XMLPrescaleTableRows struct {
	Rows []L1XMLPrescaleTableRow `xml:"row"`
}

// L1XMLPrescaleTableRow is part of L1XML
type L1XMLPrescaleTableRow struct {
	XMLName  xml.Name `xml:"row"`
	AlgoName string
	Values   []float64
}

// L1XMLPrescaleTableColumns is part of L1XML
type L1XMLPrescaleTableColumns struct {
	Columns []string
}

// UnmarshalXML takes a raw string "algo/prescale-index, 0:Cosmics, 1:..." and turns it into column names
func (r *L1XMLPrescaleTableColumns) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	values := make([]string, 0)

	for {
		tok, err := d.Token()
		if tok == nil {
			break
		}
		if err != nil {
			return err
		}
		switch se := tok.(type) {
		case xml.CharData:
			clean := strings.TrimSpace(string(se))
			if clean != "" {
				pieces := strings.Split(clean, ",")
				for _, s := range pieces[1:] {
					raw := strings.TrimSpace(s)
					pieces := strings.SplitN(raw, ":", 2)
					if len(pieces) == 1 {
						values = append(values, raw)
					} else {
						values = append(values, pieces[1])
					}
				}
			}
		}
	}
	r.Columns = values
	return nil
}

// UnmarshalXML takes the raw input string "algoname, value, value, ..." and turns it into usable data
func (r *L1XMLPrescaleTableRow) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	values := make([]float64, 0)

	for {
		tok, err := d.Token()
		if tok == nil {
			break
		}
		if err != nil {
			return err
		}
		switch se := tok.(type) {
		case xml.CharData:
			clean := strings.TrimSpace(string(se))
			if clean != "" {
				pieces := strings.Split(clean, ",")
				r.AlgoName = pieces[0]
				for _, s := range pieces[1:] {
					f, err := strconv.ParseFloat(strings.TrimSpace(s), 64)
					if err != nil {
						return err
					}
					values = append(values, f)
				}
			}
		}
	}
	r.Values = values

	return nil
}
