package types

// ColumnsJSON represents columns.json
type ColumnsJSON struct {
	Columns []JSONColumn `json:"columns"`
}

// JSONColumn is part of ColumnsJSON
type JSONColumn struct {
	Name string `json:"name"`
}
