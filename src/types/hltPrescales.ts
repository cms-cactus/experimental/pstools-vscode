import { L1Prescales } from "./l1Prescales";

export interface HLTPrescales extends L1Prescales {
  seeds: HLTSeed[];
}

export interface HLTSeed {
  "//description": string | undefined;
  "//": string | undefined;
  type: "OR" | "AND";
  refs: HLTSeedRef[];
}

export type HLTSeedRef = HLTSeed | string;
