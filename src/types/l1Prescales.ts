export interface L1Prescales {
  parent: string;
  algorithms: Record<string, L1Algo>;
}

export interface L1Algo {
  columns: number[];
}
