FROM gitlab-registry.cern.ch/cms-cactus/ops/auto-devops/basics-c8:tag-0.0.3
LABEL maintainer="Cactus <cactus@cern.ch>"

ENV NODE_VERSION=12.16.1
ENV PATH="$PATH:/usr/local/lib/nodejs/node-v${NODE_VERSION}-linux-x64/bin"

RUN curl -o node.tar.xz https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.xz && \
    mkdir -p /usr/local/lib/nodejs && tar -xJvf node.tar.xz -C /usr/local/lib/nodejs && rm node.tar.xz