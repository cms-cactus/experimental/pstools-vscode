// eslint-disable-next-line @typescript-eslint/no-var-requires
const commonRules = require("./.eslintrc.commonrules");
module.exports = {
  root: true,
  env: {
    node: true,
    es6: true,
  },
  parser: "@typescript-eslint/parser",
  // parser: require.resolve('vue-eslint-parser'),
  parserOptions: {
    ecmaVersion: 2020,
    sourceType: "module",
    project: "./tsconfig.json",
  },
  plugins: ["@typescript-eslint"],
  extends: ["eslint:recommended","plugin:@typescript-eslint/recommended"],
  overrides: [{
    files: ["src/panels/**"],
    env: {
      browser: true,
    },
  }],
  rules: Object.assign({}, commonRules, {
    "no-unused-vars": "off",
    "@typescript-eslint/no-unused-vars": ["error", { argsIgnorePattern: "^_" }],
    "@typescript-eslint/no-unused-vars-experimental": 2,
    // "@typescript-eslint/class-name-casing": "warn",
    // "@typescript-eslint/semi": "warn",
  }),
};
